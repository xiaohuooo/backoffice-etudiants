## ui

https://assets.adobe.com/id/urn:aaid:sc:EU:0b2419bc-6954-4de3-a7a1-1a776091da30?view=published

## antd-vue 组件

https://1x.antdv.com/components/layout-cn/#components-layout-demo-responsive

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
