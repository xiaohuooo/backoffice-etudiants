import Vue from 'vue'
import Router from 'vue-router'
import decole from '@/components/decole';
import enseignements from '@/components/enseignements';
import evolitions from '@/components/evolitions';
import messages from '@/components/messages';
import news from '@/components/news';
import planning from '@/components/planning';
import likes from '@/components/enseignements/likes.vue'
import enseignementsright from '@/components/enseignements/enseignementsright.vue'
Vue.use(Router)


export default new Router({
    routes: [
      {
        path:'/',
        redirect:'/enseignements'
      },
  
      {
        path: '/enseignements',
        name: 'enseignements',
        components: {
          default:enseignements,
          right:enseignementsright
        },
        children: [
                {
                  path:'/enseignements',
                  redirect:'/enseignements/likes'
                },
                {
                    name: 'likes',
                    path: 'likes',//无需添加斜杠
                    component:likes
                },
                {
                    name: 'cours',
                    path: 'cours',//无需添加斜杠
                    component: () => import('@/components/enseignements/cours.vue')
                },
                {
                  name: 'passes',
                  path: 'passes',//无需添加斜杠
                  // component: () => import('@/components/enseignements/passes.vue')
                  component: () => import('@/components/enseignements/cours.vue')

              },
            ]
      },
      {
        path: '/decole',
        name: 'decole',
        components: {
          default:decole,
          right:enseignementsright
        }
      },
      {
        path: '/evolitions',
        name: 'evolitions',
        components: {
          default:evolitions,
          right:enseignementsright
        },
        children: [
          {
            path:'/evolitions',
            redirect:'/evolitions/evolition'
          },
          {
            name: 'evolition',
            path: 'evolition',//无需添加斜杠
            component:() => import('@/components/evolitions/evolitions.vue')
          },
          {
            name: 'orientation',
            path: 'orientation',//无需添加斜杠
            component:() => import('@/components/evolitions/orientation.vue')
          },
        ]
      },
      {
        path: '/messages',
        name: 'messages',
        components: {
          default:messages,
          right:enseignementsright
        }
      },
      {
        path: '/news',
        name: 'news',
        components:{ 
          default:news,
          right:enseignementsright
        }
      },
      {
        path: '/planning',
        name: 'planning',
        components: {
          default:planning,
          right:enseignementsright
        },
        children: [
          {
            path:'/planning',
            redirect:'/planning/personnel'
          },
          {
              name: 'personnel',
              path: 'personnel',//无需添加斜杠
              component:() => import('@/components/planning/personnel.vue')
          },
          {
              name: 'cours',
              path: 'cours',//无需添加斜杠
              component: () => import('@/components/planning/cours.vue')
          }
      ]
      },
      {
        path: '/evenements',
        name: 'evenements',
        components: {
          default:() => import('@/components/evenements'),
          right:enseignementsright
        },
        children: [
          {
            path:'/evenements',
            redirect:'/evenements/travail'
          },
          {
              name: 'travail',
              path: 'travail',//无需添加斜杠
              component:() => import('@/components/evenements/travail.vue')
          },
          {
              name: 'conferences',
              path: 'conferences',//无需添加斜杠
              component:() => import('@/components/evenements/travail.vue')
              // component: () => import('@/components/evenements/conferences.vue')
          }
      ]
      }
    ]
  });
  