import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 引入AntD
import Antd from 'ant-design-vue'; 
// 引入AntD的样式文件
import 'ant-design-vue/dist/antd.css';
import '@/style/style.less'
Vue.config.productionTip = false
// 将AntD注册为Vue的插件
Vue.use(Antd)
new Vue({
  render: h => h(App),
  router
}).$mount('#app')
